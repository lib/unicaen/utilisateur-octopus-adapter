<?php

namespace UnicaenUtilisateurOctopusAdapter\Entity;

use Octopus\Entity\Db\Individu;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;

class OctopusIndividu implements RechercheIndividuResultatInterface {
    
    /** @var Individu $individu */
    private $individu;

    /**
     * @return Individu
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     * @param Individu $individu
     * @return Individu
     */
    public function setIndividu($individu)
    {
        $this->individu = $individu;
        return $this->individu;
    }

    /** Fonction de l'interface RechercheIndividuResultatInterface */

    public function getId() {
        return $this->individu->getCIndividuChaine();
    }

    /** TODO changer lorsque le compte sera lié dans octopus */
    public function getUsername()
    {
        return $this->individu->getCIndividuChaine();
    }

    public function getDisplayname()
    {
        return $this->individu->__toString();
    }

    /** TODO changer lorsque le compte sera lié dans octopus */
    public function getEmail()
    {
        return "ne-pas-repondre@unicaen.fr";
    }
}